package main

import (
	"bytes"
	monitoring "cloud.google.com/go/monitoring/apiv3/v2"
	"cloud.google.com/go/storage"
	"context"
	"encoding/json"
	"fmt"
	"google.golang.org/api/iterator"
	"google.golang.org/api/transport"
	monitoringpb "google.golang.org/genproto/googleapis/monitoring/v3"
	"io"
	"os"
	"time"
)

type bucketInfo struct {
	ID            string    `json:"id"`
	Name          string    `json:"name"`
	ProjectNumber string    `json:"projectNumber"`
	Location      string    `json:"location"`
	StorageClass  string    `json:"storageClass"`
	TimeCreated   time.Time `json:"timeCreated"`
	Updated       time.Time `json:"updated"`
	Versioning    struct {
		Enabled bool `json:"enabled"`
	} `json:"versioning"`
	RetentionPolicy struct {
		RetentionPeriod string    `json:"retentionPeriod"`
		EffectiveTime   time.Time `json:"effectiveTime"`
		IsLocked        bool      `json:"isLocked"`
	} `json:"retentionPolicy"`
	IamConfiguration struct {
		BucketPolicyOnly struct {
			Enabled bool `json:"enabled"`
		} `json:"bucketPolicyOnly"`
		UniformBucketLevelAccess struct {
			Enabled bool `json:"enabled"`
		} `json:"uniformBucketLevelAccess"`
		PublicAccessPrevention string `json:"publicAccessPrevention"`
	} `json:"iamConfiguration"`
	LocationType string `json:"locationType"`
	Rpo          string `json:"rpo"`
}

var (
	bucketName  = ""
	projectName = ""
)

func callStorageApiCustom(ctx context.Context, bckt string) (string, bool, time.Time) {
	c, _, err := transport.NewHTTPClient(ctx)
	if err != nil {
		panic(err)
	}
	url := fmt.Sprintf("https://storage.googleapis.com/storage/v1/b/%s", bckt)
	res, err := c.Get(url)
	if err != nil {
		panic(err)
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		panic(err)
	}
	var bi bucketInfo
	if err := json.Unmarshal(body, &bi); err != nil {
		panic(err)
	}
	retentionPeriod := bi.RetentionPolicy.RetentionPeriod
	if retentionPeriod == "" {
		retentionPeriod = "None"
	}
	return retentionPeriod, bi.Versioning.Enabled, bi.Updated.UTC()
}

func bucketSize(ctx context.Context) {

	c, err := monitoring.NewQueryClient(ctx)
	if err != nil {
		fmt.Println(err)
	}
	defer c.Close()

	req := &monitoringpb.QueryTimeSeriesRequest{
		Name:  projectName,
		Query: "fetch gcs_bucket | metric 'storage.googleapis.com/storage/total_bytes' | \ngroup_by 1h, [value_total_bytes_max: max(value.total_bytes)] | \nevery 1h",
	}
	csv := "project_name,bucket_name,region,storage_class,size_bytes,last_updated,versioning,retention\n"
	it := c.QueryTimeSeries(ctx, req)
	for {
		resp, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			// TODO: Handle error.
		}
		projectNm := resp.LabelValues[0].GetStringValue()
		buckNm := resp.LabelValues[1].GetStringValue()
		region := resp.LabelValues[2].GetStringValue()
		storageClass := resp.LabelValues[3].GetStringValue()
		buckSz := resp.PointData[0].Values[0].GetDoubleValue()
		retention, versioning, updated := callStorageApiCustom(ctx, buckNm)
		newLine := fmt.Sprintf("%s,%s,%s,%s,%f,%v,%v,%v\n", projectNm, buckNm, region, storageClass, buckSz, updated, versioning, retention)
		csv = csv + newLine
		//fmt.Println(csv)
	}
	uploadBucket(ctx, csv)
}

func uploadBucket(ctx context.Context, csv string) {
	client, err := storage.NewClient(ctx)
	if err != nil {
		panic(err)
	}
	defer client.Close()

	b := []byte(csv)
	buf := bytes.NewBuffer(b)

	ctx2, cancel := context.WithTimeout(ctx, time.Second*50)
	defer cancel()

	// Upload an object with storage.Writer.
	wc := client.Bucket(bucketName).Object("all_buckets_with_size.csv").NewWriter(ctx2)
	wc.ChunkSize = 0 // note retries are not supported for chunk size 0.

	if _, err = io.Copy(wc, buf); err != nil {
		panic(err)
	}
	// Data can continue to be added to the file until the writer is closed.
	if err := wc.Close(); err != nil {
		panic(err)
	}
	fmt.Println("CSV updaloaded.")
}

func main() {
	start := time.Now()
	fmt.Printf("Starting at: %s", start)
	ctx := context.Background()
	bucketName = os.Getenv("BUCKETNAME")
	projectName = os.Getenv("PROJECTNAME")
	if bucketName == "" || projectName == "" {
		panic("Env variable not set.")
	}
	bucketSize(ctx)
	elapsed := time.Since(start)
	fmt.Printf("Time it take: %s", elapsed)
}
